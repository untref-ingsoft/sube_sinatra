# Servicio web SUBE

Implementar una API REST para las siguientes funcionalidades para la tarjeta SUBE:

- Carga de saldo
- Consulta de saldo
- Pago de pasaje
